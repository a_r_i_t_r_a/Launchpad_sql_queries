# Launchpad_sql_queries

1.*Write a query which will display the customer id, account type they hold, their account number and bank name.
A. SELECT A.CUSTOMER_ID, A.ACCOUNT_TYPE, A.ACCOUNT_NO, B.BANK_NAME FROM BANK_INFO B INNER JOIN ACCOUNT_INFO A ON B.IFSC_CODE=A.IFSC_CODE;

2.*Write a query which will display the customer id, account type and the account number of HDFC customers who registered after 12-JAN-2012 and before 04-APR-2012.
A. SELECT A.CUSTOMER_ID, A.ACCOUNT_TYPE, A.ACCOUNT_NO FROM ACCOUNT_INFO A INNER JOIN BANK_INFO B ON A.IFSC_CODE=B.IFSC_CODE WHERE A.REGISTRATION_DATE BETWEEN TO_DATE('2012-01-13','YYYY-MM-DD') AND TO_DATE('2012-04-03','YYYY-MM-DD') AND B.BANK_NAME='HDFC';

3.*Write a query which will display the customer id, customer name, account no, account type and bank name where the customers hold the account.
A. SELECT C.CUSTOMER_ID, C.CUSTOMER_NAME, A.ACCOUNT_NO, A.ACCOUNT_TYPE, B.BANK_NAME FROM BANK_INFO B INNER JOIN ACCOUNT_INFO A ON B.IFSC_CODE=A.IFSC_CODE INNER JOIN CUSTOMER_PERSONAL_INFO C ON C.CUSTOMER_ID=A.CUSTOMER_ID WHERE A.ACCOUNT_NO IS NOT NULL;

4.*Write a query which will display the customer id, customer name, gender, marital status along with the unique reference string and
 sort the records based on customer id in descending order. <br/>
<br/><b>Hint:  </b>Generate unique reference string as mentioned below
:
<br/> CustomerName_Gender_MaritalStatus
<br/><b> Example, </b>
<br/> C-005           KUMAR              M                 SINGLE            KUMAR_M_SINGLE
<BR/> 
Use ""UNIQUE_REF_STRING"" as alias name for displaying the unique reference string."
A. SELECT CUSTOMER_ID, CUSTOMER_NAME, GENDER, MARITAL_STATUS, (CUSTOMER_NAME ||'_'||GENDER||'_'||MARITAL_STATUS) AS UNIQUE_REF_STRING FROM CUSTOMER_PERSONAL_INFO ORDER BY CUSTOMER_ID DESC;

5.*Write a query which will display the account number, customer id, registration date, initial deposit amount of the customer
 whose initial deposit amount is within the range of Rs.15000 to Rs.25000.
A.SELECT ACCOUNT_NO, CUSTOMER_ID, REGISTRATION_DATE, INITIAL_DEPOSIT FROM ACCOUNT_INFO WHERE INITIAL_DEPOSIT BETWEEN 15000 AND 25000;

6.*Write a query which will display customer id, customer name, date of birth, guardian name of the customers whose name starts with 'J'.
A. SELECT CUSTOMER_ID, CUSTOMER_NAME, DATE_OF_BIRTH, GUARDIAN_NAME FROM CUSTOMER_PERSONAL_INFO WHERE CUSTOMER_NAME LIKE 'J%';

7.*Write a query which will display customer id, account number and passcode. 
<br/>
Hint:  To generate passcode, join the last three digits of customer id and last four digit of account number.
 
<br/>Example
<br/>C-001                   1234567898765432                0015432
<br/>Use ""PASSCODE"" as alias name for displaying the passcode."
A. SELECT CUSTOMER_ID, ACCOUNT_NO, CONCAT(SUBSTR(CUSTOMER_ID,-3),SUBSTR(ACCOUNT_NO,-4))AS PASSCODE FROM ACCOUNT_INFO;

8.*Write a query which will display the customer id, customer name, date of birth, Marital Status, Gender, Guardian name, 
contact no and email id of the customers whose gender is male 'M' and marital status is MARRIED.
A. SELECT CUSTOMER_ID, CUSTOMER_NAME, DATE_OF_BIRTH, MARITAL_STATUS, GENDER, GUARDIAN_NAME, CONTACT_NO, MAIL_ID FROM CUSTOMER_PERSONAL_INFO WHERE GENDER='M' AND MARITAL_STATUS='MARRIED';

9.*Write a query which will display the customer id, customer name, guardian name, reference account holders name of the customers 
who are referenced / referred by their 'FRIEND'.
A. SELECT C.CUSTOMER_ID, C.CUSTOMER_NAME, C.GUARDIAN_NAME, D.REFERENCE_ACC_NAME AS FRIEND FROM CUSTOMER_PERSONAL_INFO C INNER JOIN CUSTOMER_REFERENCE_INFO D ON C.CUSTOMER_ID=D.CUSTOMER_ID;

10.*Write a query to display the customer id, account number and interest amount in the below format with INTEREST_AMT as alias name
 Sort the result based on the INTEREST_AMT in ascending order.  <BR/>Example: 
$5<BR/>Hint: Need to prefix $ to interest amount and round the result without decimals.
A. SELECT CUSTOMER_ID, ACCOUNT_NO, CONCAT('$',ROUND(INTEREST,0)) AS INTEREST_AMT FROM ACCOUNT_INFO ORDER BY INTEREST_AMT;
 
11.*Write a query which will display the customer id, customer name, account no, account type, activation date,
 bank name whose account will be activated on '10-APR-2012'
A. SELECT C.CUSTOMER_ID, C.CUSTOMER_NAME, A.ACCOUNT_NO, A.ACCOUNT_TYPE, A.ACTIVATION_DATE, B.BANK_NAME FROM CUSTOMER_PERSONAL_INFO C INNER JOIN ACCOUNT_INFO A ON C.CUSTOMER_ID=A.CUSTOMER_ID INNER JOIN BANK_INFO B ON B.IFSC_CODE=A.IFSC_CODE WHERE A.ACTIVATION_DATE=TO_DATE('2012-04-10','YYYY-MM-DD');

12.*Write a query which will display account number, customer id, customer name, bank name, branch name, ifsc code
, citizenship, interest and initial deposit amount of all the customers.
A.SELECT A.ACCOUNT_NO, C.CUSTOMER_ID, C.CUSTOMER_NAME, B.BANK_NAME, B.BRANCH_NAME, B.IFSC_CODE, C.CITIZENSHIP, A.INTEREST, A.INITIAL_DEPOSIT FROM CUSTOMER_PERSONAL_INFO C INNER JOIN ACCOUNT_INFO A ON C.CUSTOMER_ID=A.CUSTOMER_ID INNER JOIN BANK_INFO B ON B.IFSC_CODE=A.IFSC_CODE;

13.*Write a query which will display customer id, customer name, date of birth, guardian name, contact number,
 mail id and reference account holder's name of the customers who has submitted the passport as an identification document.
A. SELECT C.CUSTOMER_ID, C.CUSTOMER_NAME, C.DATE_OF_BIRTH, C.GUARDIAN_NAME, C.MAIL_ID, D.REFERENCE_ACC_NAME FROM CUSTOMER_PERSONAL_INFO C INNER JOIN CUSTOMER_REFERENCE_INFO D ON C.CUSTOMER_ID=D.CUSTOMER_ID WHERE C.IDENTIFICATION_DOC_TYPE='PASSPORT';

14.*Write a query to display the customer id, customer name, account number, account type, initial deposit, 
interest who have deposited maximum amount in the bank.
A. SELECT * FROM (SELECT C.CUSTOMER_ID, C.CUSTOMER_NAME, A.ACCOUNT_NO, A.ACCOUNT_TYPE, A.INITIAL_DEPOSIT, A.INTEREST FROM CUSTOMER_PERSONAL_INFO C INNER JOIN ACCOUNT_INFO A ON C.CUSTOMER_ID = A.CUSTOMER_ID GROUP BY C.CUSTOMER_ID, C.CUSTOMER_NAME, A.ACCOUNT_NO, A.ACCOUNT_TYPE, A.INTEREST, A.INITIAL_DEPOSIT ORDER BY A.INITIAL_DEPOSIT DESC) WHERE ROWNUM=1

15.*Write a query to display the customer id, customer name, account number, account type, interest, bank name 
and initial deposit amount of the customers who are getting maximum interest rate.
A. SELECT * FROM (SELECT C.CUSTOMER_ID, C.CUSTOMER_NAME, A.ACCOUNT_NO, A.ACCOUNT_TYPE, A.INTEREST, B.BANK_NAME, A.INITIAL_DEPOSIT FROM CUSTOMER_PERSONAL_INFO C INNER JOIN ACCOUNT_INFO A ON C.CUSTOMER_ID = A.CUSTOMER_ID INNER JOIN BANK_INFO B ON A.IFSC_CODE = B.IFSC_CODE GROUP BY C.CUSTOMER_ID, C.CUSTOMER_NAME, A.ACCOUNT_NO, A.ACCOUNT_TYPE, A.INTEREST,B.BANK_NAME, A.INITIAL_DEPOSIT ORDER BY A.INTEREST DESC) WHERE ROWNUM=1

16.Write a query to display the customer id, customer name, account no, bank name, contact no 
and mail id of the customers who are from BANGALORE.
A. SELECT C.CUSTOMER_ID, C.CUSTOMER_NAME, A.ACCOUNT_NO, B.BANK_NAME, B.BRANCH_NAME, C.CONTACT_NO, C.MAIL_ID FROM CUSTOMER_PERSONAL_INFO C INNER JOIN ACCOUNT_INFO A ON C.CUSTOMER_ID=AICUSTOMER_ID INNER JOIN BANK_INFO B
ON B.IFSC_CODE=A.IFSC_CODE WHERE C.ADDRESS LIKE '%BANGALORE';

17.Write a query which will display customer id, bank name, branch name, ifsc code, registration date, 
activation date of the customers whose activation date is in the month of march (March 1'st to March 31'st).
A.SELECT C.CUSTOMER_ID, B.BANK_NAME, B.BRANCH_NAME, B.IFSC_CODE, A.REGISTRATION_DATE, A.ACTIVATION_DATE FROM CUSTOMER_PERSONAL_INFO C INNER JOIN ACCOUNT_INFO A ON C.CUSTOMER_ID=A.CUSTOMER_ID INNER JOIN BANK_INFO B ON B.IFSC_CODE=A.IFSC_CODE WHERE EXTRACT(MONTH FROM ACTIVATION_DATE)=03;

18.Write a query which will calculate the interest amount and display it along with customer id, customer name, 
account number, account type, interest, and initial deposit amount.<BR>Hint :Formula for interest amount, 
calculate: ((interest/100) * initial deposit amt) with column name 'interest_amt' (alias)
A. SELECT ((INTEREST/100)*INITIAL_DEPOSIT) AS INTEREST_AMT, C.CUSTOMER_ID, C.CUSTOMER_NAME, A.ACCOUNT_NO, A.ACCOUNT_TYPE, A.INTEREST, A.INITIAL_DEPOSIT FROM CUSTOMER_PERSONAL_INFO C INNER JOIN ACCOUNT_INFO A ON C.CUSTOMER_ID=A.CUSTOMER_ID;

19.Write a query to display the customer id, customer name, date of birth, guardian name, contact number, 
mail id, reference name who has been referenced by 'RAGHUL'.
A. SELECT C.CUSTOMER_ID, C.CUSTOMER_NAME, C.DATE_OF_BIRTH, C.GUARDIAN_NAME, C.CONTACT_NO, C.MAIL_ID, D.REFERENCE_ACC_NAME FROM CUSTOMER_PERSONAL_INFO C INNER JOIN CUSTOMER_REFERENCE_INFO D ON C.CUSTOMER_ID=CRI.CUSTOMER_ID WHERE D.REFERENCE_ACC_NAME='RAGHUL';

20."Write a query which will display the customer id, customer name and contact number with ISD code of 
all customers in below mentioned format.  Sort the result based on the customer id in descending order. 
<BR>Format for contact number is :  
<br/> ""+91-3digits-3digits-4digits""
<br/> Example: +91-924-234-2312
<br/> Use ""CONTACT_ISD"" as alias name."
A. SELECT CUSTOMER_ID, CUSTOMER_NAME, ('+91-'||SUBSTR(CONTACT_NO,1,3)||'-'||SUBSTR(CONTACT_NO,4,3)||'-'||SUBSTR(CONTACT_NO,-4)) AS CONTACT_ISD FROM CUSTOMER_PERSONAL_INFO ORDER BY CUSTOMER_ID DESC;

21.Write a query which will display account number, account type, customer id, customer name, date of birth, guardian name, 
contact no, mail id , gender, reference account holders name, reference account holders account number, registration date, 
activation date, number of days between the registration date and activation date with alias name "NoofdaysforActivation", 
bank name, branch name and initial deposit for all the customers.
A. SELECT A.ACCOUNT_NO, A.ACCOUNT_TYPE, C.CUSTOMER_ID, C.CUSTOMER_NAME, C.DATE_OF_BIRTH, C.GUARDIAN_NAME, C.CONTACT_NO, C.MAIL_ID, C.GENDER, D.REFERENCE_ACC_NAME, D.REFERENCE_ACC_NO, A.REGISTRATION_DATE, A.ACTIVATION_DATE, (ACTIVATION_DATE-REGISTRATION_DATE) AS NoofdaysforActivation, B.BANK_NAME, B.BRANCH_NAME, A.INITIAL_DEPOSIT FROM CUSTOMER_PERSONAL_INFO C INNER JOIN ACCOUNT_INFO A ON C.CUSTOMER_ID=A.CUSTOMER_ID
INNER JOIN BANK_INFO B ON B.IFSC_CODE=A.IFSC_CODE INNER JOIN CUSTOMER_REFERENCE_INFO D ON D.CUSTOMER_ID=C.CUSTOMER_ID;

22."Write a query which will display customer id, customer name,  guardian name, identification doc type,
 reference account holders name, account type, ifsc code, bank name and current balance for the customers 
who has only the savings account. 
<br/>Hint:  Formula for calculating current balance is add the intital deposit amount and interest
 and display without any decimals. Use ""CURRENT_BALANCE"" as alias name."
A. SELECT C.CUSTOMER_ID, C.CUSTOMER_NAME, C.GUARDIAN_NAME, C.IDENTIFICATION_DOC_TYPE, D.REFERENCE_ACC_NAME, A.ACCOUNT_TYPE, B.IFSC_CODE, B.BANK_NAME,(INITIAL_DEPOSIT+(INTEREST/100)*INITIAL_DEPOSIT) AS CURRENT_BALANCE FROM CUSTOMER_PERSONAL_INFO C INNER JOIN ACCOUNT_INFO A ON C.CUSTOMER_ID=A.CUSTOMER_ID INNER JOIN BANK_INFO B ON B.IFSC_CODE=A.IFSC_CODE INNER JOIN CUSTOMER_REFERENCE_INFO D ON D.CUSTOMER_ID=C.CUSTOMER_ID WHERE ACCOUNT_TYPE='SAVINGS';

23."Write a query which will display the customer id, customer name, account number, account type, interest, initial deposit;
 <br/>check the initial deposit,<br/> if initial deposit is 20000 then display ""high"",<br/> if initial deposit is 16000 display 'moderate'
,<br/> if initial deposit is 10000 display 'average', <br/>if initial deposit is 5000 display 'low', <br/>if initial deposit is 0 display
 'very low' otherwise display 'invalid' and sort by interest in descending order.<br/>
Hint: Name the column as ""Deposit_Status"" (alias). 
<br/>Strictly follow the lower case for strings in this query."
A. SELECT C.CUSTOMER_ID, C.CUSTOMER_NAME, A.ACCOUNT_NO, A.ACCOUNT_TYPE, A.INTEREST, A.INITIAL_DEPOSIT, CASE WHEN INITIAL_DEPOSIT=20000 THEN 'high' WHEN INITIAL_DEPOSIT=16000 THEN 'moderate' WHEN INITIAL_DEPOSIT=10000 THEN 'average' WHEN INITIAL_DEPOSIT=5000 THEN 'low' WHEN INITIAL_DEPOSIT=0 THEN 'very low' ELSE 'invalid' END DEPOSIT_STATUS FROM CUSTOMER_PERSONAL_INFO C INNER JOIN ACCOUNT_INFO A ON C.CUSTOMER_ID=A.CUSTOMER_ID ORDER BY A.INTEREST;


24."Write a query which will display customer id, customer name,  account number, account type, bank name, ifsc code, initial deposit amount
 and new interest amount for the customers whose name starts with ""J"". 
<br/> Hint:  Formula for calculating ""new interest amount"" is 
if customers account type is savings then add 10 % on current interest amount to interest amount else display the current interest amount.
 Round the new interest amount to 2 decimals.<br/> Use ""NEW_INTEREST"" as alias name for displaying the new interest amount.

<br/>Example, Assume Jack has savings account and his current interest amount is 10.00, then the new interest amount is 11.00 i.e (10 + (10 * 10/100)). 
"
A. SELECT CPI.CUSTOMER_ID, CPI.CUSTOMER_NAME, AI.ACCOUNT_NO, AI.ACCOUNT_TYPE, BI.BANK_NAME, BI.IFSC_CODE, AI.INITIAL_DEPOSIT, (CASE WHEN ACCOUNT_TYPE='SAVINGS' THEN ROUND((INTEREST+(INTEREST*10/100)),2) ELSE INTEREST END) NEW_INTEREST FROM CUSTOMER_PERSONAL_INFO CPI INNER JOIN ACCOUNT_INFO AI ON CPI.CUSTOMER_ID=AI.CUSTOMER_ID INNER JOIN BANK_INFO BI ON BI.IFSC_CODE=AI.IFSC_CODE WHERE CPI.CUSTOMER_NAME LIKE 'J%';

25.Write query to display the customer id, customer name, account no, initial deposit, tax percentage as calculated below.
<BR>Hint: <BR>If initial deposit = 0 then tax is '0%'<BR>If initial deposit &lt;= 10000 then tax is '3%' 
<BR>If initial deposit &gt; 10000 and initial deposit &lt; 20000 then tax is '5%' <BR>If initial deposit &gt;= 20000 and
 initial deposit&lt;=30000 then tax is '7%' <BR>If initial deposit &gt; 30000 then tax is '10%' <BR>Use the alias name 'taxPercentage'
A. SELECT C.CUSTOMER_ID, C.CUSTOMER_NAME, A.ACCOUNT_NO, A.INITIAL_DEPOSIT, (CASE WHEN INITIAL_DEPOSIT=0 THEN '0%' WHEN INITIAL_DEPOSIT<=10000 THEN '3%' WHEN INITIAL_DEPOSIT>10000 AND INITIAL_DEPOSIT<20000 THEN '5%' WHEN INITIAL_DEPOSIT>=20000 AND INITIAL_DEPOSIT<=30000 THEN '7%' WHEN INITIAL_DEPOSIT>30000 THEN '10%' END)taxPercentage FROM CUSTOMER_PERSONAL_INFO C INNER JOIN ACCOUNT_INFO A ON C.CUSTOMER_ID=A.CUSTOMER_ID;
